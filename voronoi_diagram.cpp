#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <vector>
#include <map>
#include <CGAL/Arr_walk_along_line_point_location.h>                            
#include <CGAL/intersections.h>
#include "voronoi_diagram.h"
#include "arr_print.h"

#define SCATTER_POINTS 5

// Internal: get the midpoint of two points
//
// p1 - Euclidian point in space
// p2 - Euclidian point in space
// 
// TODO: add support for not using double
//
// Return Point_2 that holds the midpoint of p1 and p2
Point_2 calculate_midpoint(Point_2 p1, Point_2 p2)
{
        double x1 = to_double(p1.x());
        double x2 = to_double(p2.x());
        double y1 = to_double(p1.y());
        double y2 = to_double(p2.y());
        return Point_2( (x1+x2)/2.0, (y1+y2)/2.0);
}

// Internal: find the location of face handle f in Arrangement_2 vd
//
// vd - structure that hold the edges and points defining the VD
// f - face in vd
//
// Returns int for the location of f in the vector of vd for face handles
int location(Arrangement_2* vd, Arrangement_2::Face_const_handle f)
{
        Arrangement_2::Face_const_iterator fit;
        int fl = 0;
        for (fit = vd->faces_begin(); fit != vd->faces_end(); ++fit){
                if(fit == f)
                        break;
                fl++;
        }
        return fl;
}

// Internal: Perform a point-location query on the resulting arrangement and print      
// the boundary of the face that contains it.                                 
//
// vd - structure that hold the edges and points defining the VD
// q - point that we are trying to find the face it belongs to
//
// Returns nothing
Face_const_handle locate_face_from_point(Arrangement_2* vd, Point_2 q)
{
        Walk_pl        pl(*vd);                                                       
        CGAL::Object   obj = pl.locate (q);                                           
        Face_const_handle  f;                                          
        CGAL::assign (f, obj);         
        return f;
//        std::cout << "The query point (" << q << ") is located in: ";                 
//        print_face<Arrangement_2> (f);                         
}

// Internal: get the bisector between the two points
//
// p1 - point in 2D space 
// p2 - point in 2D space 
//
// Returns a line that bisects p1 and p2
Line_2 get_bisector(Point_2 p1, Point_2 p2)
{
        CGAL::Quotient<CGAL::MP_Float> rise = (p2.y() - p1.y());
        CGAL::Quotient<CGAL::MP_Float> run =  (p2.x() - p1.x());
//        CGAL::Quotient<double> rise = (p2.y() - p1.y());
 //       CGAL::Quotient<double> run =  (p2.x() - p1.x());
        Point_2 mpt = calculate_midpoint(p1, p2);

        if(rise == 0 ){
                printf("Warning: rise == 0\n");
                return Line_2(mpt, Point_2(mpt.x(), mpt.y() + 1));
        }
        if(run == 0){
                printf("Warning: run == 0\n");
                return Line_2(mpt, Point_2(mpt.x() + 1, mpt.y()));
        }

//        CGAL::Quotient<double> slope = rise/run; 
//        CGAL::Quotient<double> slope_inv = -1.0/slope;
       
//        CGAL::Quotient<double> b = mpt.y() - slope_inv * mpt.x();
        CGAL::Quotient<CGAL::MP_Float> slope = rise/run; 
        CGAL::Quotient<CGAL::MP_Float> slope_inv = -1.0/slope;
       
        CGAL::Quotient<CGAL::MP_Float> b = mpt.y() - slope_inv * mpt.x();
        return Line_2(slope_inv, -1.0, b);
}

void voronoi_insert_bisec_intersections( Arrangement_2* vd, 
                                         Line_2 bisec,
                                         int current_face,
                                         std::vector<Arrangement_2::Face_const_handle>& faces,
                                         std::vector<CGAL::Segment_2<Kernel> >& edges)
{
        // get edges that go around face
        Arrangement_2::Ccb_halfedge_const_circulator curr = faces[current_face]->outer_ccb();

        int i = 0; 
        CGAL::Point_2<Kernel> pts[2];
        // find the intersecting edge and the corresponding points
        do {
                Point_2 p1 = curr->source()->point();
                Point_2 p2 = curr->target()->point();
                // X-monotone segment_2 does not work
                CGAL::Segment_2<Kernel> face_edge(p1, p2);

                CGAL::Object result = CGAL::intersection(face_edge, bisec);
                if (const CGAL::Point_2<Kernel> *ipoint = CGAL::object_cast<CGAL::Point_2<Kernel> >(&result)) {
                        Arrangement_2::Halfedge_const_handle edge = curr->twin();
                        Arrangement_2::Face_const_handle face = edge->face();
                        if (!face->is_unbounded()){
                                bool found = false;
                                for(int j = 0; j < faces.size(); j++)
                                        if(faces[j] == face)
                                               found = true; 
                                if(found == false)
                                        faces.push_back(face);
                        }

                        pts[i] = *ipoint; 
                        i++;
                }

        } while (++curr != faces[current_face]->outer_ccb());

        Segment_2 newseg(pts[0], pts[1]); 
        edges.push_back(newseg); 
}

Point_2 center(Arrangement_2::Face_handle f)
        {
                double sum_x = 0;
                double sum_y = 0;
                double signedArea = 0;

                double x0 = 0.0; // Current vertex X
                double y0 = 0.0; // Current vertex Y
                double x1 = 0.0; // Next vertex X
                double y1 = 0.0; // Next vertex Y
                double a = 0.0;  // Partial signed area

                std::vector<Point_2> vertices;
                Arrangement_2::Ccb_halfedge_circulator edge = f->outer_ccb();
                do{
                        vertices.push_back(edge->source()->point()); 
                        vertices.push_back(edge->target()->point()); 
                        edge++;
                }while( edge != f->outer_ccb());

                int i; 
                for(i = 0; i < vertices.size() - 1; i++){
                
                        x0 = to_double(vertices[i].x());
                        y0 = to_double(vertices[i].y());
                        x1 = to_double(vertices[i+1].x());
                        y1 = to_double(vertices[i+1].y());
                        a = x0*y1 - x1*y0;
                        signedArea += a;
                        sum_x += (x0 + x1)*a;
                        sum_y += (y0 + y1)*a;
                }
                // Do last vertex
                x0 = to_double(vertices[i].x());
                y0 = to_double(vertices[i].y());
                x1 = to_double(vertices[0].x());
                y1 = to_double(vertices[0].y());
                a = x0*y1 - x1*y0;
                signedArea += a;
                sum_x += (x0 + x1)*a;
                sum_y += (y0 + y1)*a;

                signedArea *= 0.5;
                sum_x /= (6.*signedArea);
                sum_y /= (6.*signedArea);
              
                return Point_2(sum_x, sum_y); 
       }
// Internal: create a new voronoi cell within a voronoi diagram
//
// vd - structure that hold the edges and points defining the VD
// face_to_sites - a map from index of a face in vd to a Point_3 site
// current_face_location - index of current face in list of face_handles
// p - current point being inserted into the VD
//
// Returns nothing
void voronoi_create_cell(Arrangement_2* vd, std::vector<Point_3> sd, Face_const_handle current_face, 
                         Point_3 p, int current_sd)
{
        Point_2 current_point(p.x(), p.y()); 

        int i;
        for(i = 0; i < current_sd; i++){
                Face_const_handle face = locate_face_from_point(vd, Point_2(sd[i].x(), sd[i].y()));
                if(current_face == face)
                        break;
        }
        Point_2 current_tile_point(sd[i].x(), sd[i].y());

        std::vector<Arrangement_2::Face_const_handle> faces; 
        std::vector<CGAL::Segment_2<Kernel> > edges; 
        int current_face_idx = 0;
        Observer obs(*vd, current_point, current_sd, sd); 

        faces.push_back(current_face);

        do {
                // find bisector with current face and current point
                Line_2 bisec = get_bisector( current_point, current_tile_point); 

                voronoi_insert_bisec_intersections(vd, bisec, current_face_idx, faces, edges);
                current_face_idx++;
                for(i = 0; i < current_sd; i++){
                        Face_const_handle face = locate_face_from_point(vd, Point_2(sd[i].x(), sd[i].y()));
                        if(faces[current_face_idx] == face)
                                break;
                }
                current_tile_point = Point_2(sd[i].x(), sd[i].y());
                
        } while(current_face_idx < faces.size() );

        for(int i = 0; i < edges.size();i++)
                CGAL::insert(*vd, edges[i]);

        std::vector<Arrangement_2::Halfedge_handle> e;
        std::vector<Arrangement_2::Face_handle> faces_added;
        std::vector<Arrangement_2::Face_handle> faces_split;
        obs.get_all(e, faces_split, faces_added);

       /* double xmin = 10000;
        int min = 0;
        for(int p=0; p < faces_added.size(); p++){
                Point_2 tmp = center(faces_added[p]);
                if(to_double(tmp.x()) < xmin) {
                        xmin = to_double(tmp.x());
                        min = p;
                }
        }
*/


       for(int k = faces_added.size() - 1; k < faces_added.size(); k++){
                Arrangement_2::Ccb_halfedge_const_circulator curr = faces_added[0]->outer_ccb();
                curr++;
                while(curr != faces_added[0]->outer_ccb()){
                        for(int a = 0; a < faces_added.size(); a++)
                                if(curr->twin()->face() == faces_added[a]){
                                        vd->remove_edge(vd->non_const_handle(curr));
                                        Arrangement_2::Face_handle test;
                                        obs.get_merged_face(test);
                                        curr = test->outer_ccb();
                                        curr++;
                                }
                        curr++;
                }
/*                 // Print the boundary of each of the holes.
                Arrangement_2::Hole_const_iterator hi;
                for (hi = curr->face()->holes_begin(); hi != curr->face()->holes_end(); ++hi) {

                        Arrangement_2::Ccb_halfedge_const_circulator curr = *hi;
                        do {
                                Arrangement_2::Halfedge_const_handle he = curr;
                                vd->remove_edge(vd->non_const_handle(he));
                        } while (++curr != *hi); 
                }
*/
        }
        
}

// Internal: insert a single Point_3 into the Voronoi Diagram
//
// vd - structure that hold the edges and points defining the VD
// face_to_sites - a map from index of a face in vd to a Point_3 site
// p - point to be insert into VD
//
// Returns nothing
void voronoi_insert(Arrangement_2* vd, std::vector<Point_3> sd, Point_3 p, int current_sd)
{

        // First point into the vd
        if(vd->is_empty()) {                                                                              
                Segment_2       cv[4];
                Point_2 p1(0,0), p2(1,0), p3(1,1),p4(0,1);
                cv[0] = Segment_2 (p1, p2); 
                cv[1] = Segment_2 (p2, p3); 
                cv[2] = Segment_2 (p3, p4); 
                cv[3] = Segment_2 (p4, p1); 
                CGAL::insert(*vd, &cv[0], &cv[4]);

//                Face_const_handle current_face = locate_face_from_point(vd, Point_2(p.x(), p.y()));
//                int face_location = location(vd, current_face);
//                face_to_sites[face_location] = p; 
        } else {
                Face_const_handle current_face = locate_face_from_point(vd, Point_2(p.x(), p.y()));

                if(current_face->is_unbounded())
                        std::cout << "Error: unbounded point" << std::endl;
                else {
                        voronoi_create_cell(vd, sd, current_face, p, current_sd);

//                        current_face = locate_face_from_point(vd, Point_2(p.x(), p.y()));
//                        face_location = location(vd, current_face);
//                        face_to_sites[face_location] = p; 
                }                        
        }
}

// Public: Create a Voronoi Diagram with a set of scatter points
// 
// vd - voronoi diagram structure that hold the edges and points defining the VD
// sd - actual scattar data points used to create VD
// 
// Returns nothing 
void create_voronoi_diagram(Arrangement_2* vd, std::vector<Point_3>* sd)
{
//        std::vector<Point_3> vd_sites;
//        std::map< int, Point_3 > face_to_sites;

        // Random Points
//        srand( time( NULL ) ); 
        std::fstream file ("random.txt", std::fstream::in); 
        double x, y;
        for(int i = 0; i < SCATTER_POINTS; i++) {
               // sd->push_back( Point_3( (double)(rand() % RAND_MAX)/RAND_MAX, (double)(rand() % RAND_MAX)/RAND_MAX, 0));
               //file << (double)(rand() % RAND_MAX)/RAND_MAX << " " <<  (double)(rand() % RAND_MAX)/RAND_MAX << std::endl;
                 file >> x >> y;
                 sd->push_back( Point_3( x, y, 0));
        }
        file.close();

        for(int i = 0; i < sd->size(); i++) {
                voronoi_insert( vd, *sd , (*sd)[i], i );
        }         
}
