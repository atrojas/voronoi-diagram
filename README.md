# Voronoi Diagram 

* AUTHOR: Carlos Rojas
* VERSION: 1.0

## Description
Create voronoi diagrams and perform sibson interpolation on a scatter value
dataset.

## Algorithm
        For every randomly sampled scatter point:
                insert point P into voronoi diagram()
        visualize the voronoi diagram

        insert point P into voronoi diagram()
                find the tile P is in
                find the neighboring tiles
                create a bisector between P and all the neighboring points
                
## How To Build
        cd /to/main/dir
        mkdir build
        cd build
        cmake ..
        make

## Sipson Formula
$ f(P) = \frac{\sum_{i=1}^k a_i f_i}{\sum_{i=1}^k a_i}$

## TODO
* Add center point and a map to that face
* Insertion algorithm for V.D.
        * Locate which tile in -> if not in tile start[0,1]
        * Find neighbors        
* Sibson interpolation
* Convert to update OpenGL
* Document OpenGL code
