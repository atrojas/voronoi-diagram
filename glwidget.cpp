#include "glwidget.h"
#define value 100
#define DEG_TO_RADS 0.01745329251994329576923690768489

GLWidget::GLWidget()
{
        create_voronoi_diagram(&vd, &sd);
}

void GLWidget::initializeGL()
{
        //initialization of OpenGL
        glClearColor ( 0.85f, 0.85f, 0.85f, 0.f );
        //resizeGL( 400 , 300 );

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        //gluOrtho2D(-100, 100, -100, 100);
        gluOrtho2D(-.2, 1.2, -.2, 1.2);
}


void GLWidget::paintGL()
{
        //draw scene here
        glMatrixMode(GL_MODELVIEW);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        glColor3f(.0, .0, .0);
        
        glPointSize(4.0);
        glBegin(GL_POINTS);
        for(int i = 0; i < sd.size(); i++)
                glVertex2f(to_double(sd[i].x()), to_double(sd[i].y()));

        glEnd();

        glBegin(GL_LINES);
        Arrangement_2::Edge_const_iterator eit;
        for (eit = vd.edges_begin(); eit != vd.edges_end(); ++eit){
               glVertex2f(to_double(eit->source()->point().x()), to_double(eit->source()->point().y()));
               glVertex2f(to_double(eit->target()->point().x()), to_double(eit->target()->point().y()));
        }
        glEnd();

}

void GLWidget::resizeGL ( int width, int height )
{
}

/*void GLWidget::mousePressEvent(QMouseEvent *event)
{
  //proces mouse events for rotate/move inside 3D scene
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
 //proces keyboard events
}
*/
