#ifndef VORONOI_DIAGRAM_H
#define VORONOI_DIAGRAM_H
#include <CGAL/Cartesian.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Quotient.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_segment_traits_2.h>                                          
#include <CGAL/Arr_simple_point_location.h>                                     
#include <CGAL/Arr_observer.h>
#include <CGAL/centroid.h>
#include <vector>
#include "arr_print.h"

//typedef CGAL::Quotient<CGAL::MP_Float>     Number_type;
typedef CGAL::Quotient<CGAL::MP_Float>     Number_type;
typedef CGAL::Cartesian<Number_type>       Kernel;
typedef CGAL::Arr_segment_traits_2<Kernel> Traits_2;
typedef Traits_2::Point_2                  Point_2;
typedef Traits_2::X_monotone_curve_2       Segment_2;
typedef CGAL::Arrangement_2<Traits_2>      Arrangement_2;
typedef Arrangement_2::Face_const_handle   Face_const_handle;
typedef Kernel::Point_3                    Point_3;
typedef CGAL::Arr_walk_along_line_point_location<Arrangement_2> Walk_pl;        
typedef Kernel::Line_2                     Line_2;

void create_voronoi_diagram(Arrangement_2* vd, std::vector<Point_3>* scatter_data);

// An arrangement observer, used to receive notifications of face splits and
// face mergers.
class Observer : public CGAL::Arr_observer<Arrangement_2>
{
public:
        Observer(Arrangement_2& arr, Point_2 newP, int c, std::vector<Point_3> scatter) : CGAL::Arr_observer<Arrangement_2> (arr)
        {
                vd = arr;
                current = c;
                p = newP;
                sd = scatter;
        }

        double calculate_distance(Point_2 p1, Point_2 p2)
        {
                double x1 = to_double(p1.x());
                double y1 = to_double(p1.y());
                double x2 = to_double(p2.x());
                double y2 = to_double(p2.y());

                return sqrt( x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2);
        }

        Point_2 centroid(Face_handle f)
        {
                double sum_x = 0;
                double sum_y = 0;
                double signedArea = 0;

                double x0 = 0.0; // Current vertex X
                double y0 = 0.0; // Current vertex Y
                double x1 = 0.0; // Next vertex X
                double y1 = 0.0; // Next vertex Y
                double a = 0.0;  // Partial signed area

                std::vector<Point_2> vertices;
                Ccb_halfedge_circulator edge = f->outer_ccb();
                do{
                        vertices.push_back(edge->source()->point()); 
                        vertices.push_back(edge->target()->point()); 
                        edge++;
                }while( edge != f->outer_ccb());

                int i; 
                for(i = 0; i < vertices.size() - 1; i++){
                
                        x0 = to_double(vertices[i].x());
                        y0 = to_double(vertices[i].y());
                        x1 = to_double(vertices[i+1].x());
                        y1 = to_double(vertices[i+1].y());
                        a = x0*y1 - x1*y0;
                        signedArea += a;
                        sum_x += (x0 + x1)*a;
                        sum_y += (y0 + y1)*a;
                }
                // Do last vertex
                x0 = to_double(vertices[i].x());
                y0 = to_double(vertices[i].y());
                x1 = to_double(vertices[0].x());
                y1 = to_double(vertices[0].y());
                a = x0*y1 - x1*y0;
                signedArea += a;
                sum_x += (x0 + x1)*a;
                sum_y += (y0 + y1)*a;

                signedArea *= 0.5;
                sum_x /= (6.*signedArea);
                sum_y /= (6.*signedArea);
              
                return Point_2(sum_x, sum_y); 
       }


        virtual void before_split_face (Face_handle, Halfedge_handle e)
        {
                edges_added.push_back(e);
        }

        Face_handle location(Arrangement_2 vd, Point_2 q)
        {
                Walk_pl        pl(vd);
                CGAL::Object   obj = pl.locate (q);
                Face_handle  f;
                CGAL::assign (f, obj);
                return f;
        }

        virtual void after_split_face ( Face_handle f1, Face_handle f2, bool is_hole)
        {
/*                
                int i;
                int found_f1 = 0;
                int found_f2 = 0;
                if(current == 1) return;

                for(i = 0; i <= current - 1; i++){ 
                        std::cout << Point_2(sd[i].x(), sd[i].y()) << std::endl;
                        print_arrangement<Arrangement_2>(vd);

                        Face_handle face = location(vd, Point_2(sd[i].x(), sd[i].y()));
                        if(face->is_unbounded()) std::cout << "faf" << std::endl;

                        if(f1 == face)
                                found_f1 = 1;
                        else if(f2 == face)
                                found_f1 = 1;
                }
               
                if(!found_f1 && found_f2){
                        faces_split.push_back(f2);
                        faces_added.push_back(f1);
                }else if(!found_f2 && found_f1) {
                        faces_added.push_back(f1);
                        faces_split.push_back(f2);
                }
 */                        
                /*std::vector<Point_2> p1;
                std::vector<Point_2> p2;

                Ccb_halfedge_circulator edge = f1->outer_ccb();
                do{
                        p1.push_back(edge->source()->point());
                        p1.push_back(edge->target()->point());
                        std::cout << edge->source()->point() << std::endl;
                        std::cout << edge->targe()->point() << std::endl;
                        edge++;
                }while( edge != f1->outer_ccb());

                Point_2 cf1= CGAL::centroid(p1.begin(), p1.end(), CGAL::Dimension_tag<0>());

                Ccb_halfedge_circulator edge2= f2->outer_ccb();
                do{
                       p2.push_back(edge2->source()->point());
                       p2.push_back(edge2->target()->point());
                        edge2++;
                }while( edge2 != f2->outer_ccb());
                Point_2 cf2= CGAL::centroid(p2.begin(), p2.end(), CGAL::Dimension_tag<0>());

*/
                
                if(current == 1) return;
                //print_arrangement<Arrangement_2>(vd);
                Point_2 cf1 = centroid(f1);
                Point_2 cf2 = centroid(f2);
//                std::cout << cf1 << " " << cf2 << std::endl;                        
                double d1 = calculate_distance(cf1, p);
                double d2 = calculate_distance(cf2, p);
//                std::cout << d1 << " " << d2 << std::endl;                        
                if(d1 < d2){
                        faces_added.push_back(f1);
                        faces_split.push_back(f2);
                } else {

                        faces_split.push_back(f1);
                        faces_added.push_back(f2);
                }
        
        }

        virtual void after_merge_face ( Face_handle f)
        {
                merged_face = f;
        }

        void clear()
        {
                edges_added.erase(edges_added.begin(), edges_added.end());
                faces_added.erase(faces_added.begin(), faces_added.end());
        }
        
        void get_all(std::vector<Halfedge_handle>& e, std::vector<Face_handle>& f_split, std::vector<Face_handle>& f_added)
        {
                f_added = faces_added;
                f_split = faces_split;
                e = edges_added; 
        } 
        
        void get_merged_face(Face_handle& f)
        {
                f = merged_face;
        }
        

private:
        std::vector<Halfedge_handle> edges_added;
        std::vector<Face_handle> faces_added;
        std::vector<Face_handle> faces_split;
        std::vector<Point_3> sd;
        int current;
        Face_handle merged_face;
        Point_2 p;
        Arrangement_2 vd;
};

#endif
